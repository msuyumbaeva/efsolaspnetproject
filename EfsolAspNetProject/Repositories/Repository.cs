﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EfsolAspNetProject.Models;
using EfsolAspNetProject.Repositories.Contracts;
using Microsoft.EntityFrameworkCore;

namespace EfsolAspNetProject.Repositories
{
    public class Repository<T> : IRepository<T> where T : Entity
    {
        protected ApplicationDbContext _context;
        protected DbSet<T> DbSet { get; set; }

        public Repository(ApplicationDbContext context)
        {
            _context = context;
        }

        public void Add(T entity)
        {
            DbSet.Add(entity);
            _context.SaveChanges();
        }

        public IEnumerable<T> GetAll()
        {
            return DbSet.ToList();
        }

        public T GetById(int id)
        {
            return DbSet.FirstOrDefault(e => e.Id == id);
        }

        public void Update(T entity)
        {
            _context.Update(entity);
            _context.SaveChanges();
        }

        public void Delete(T entity)
        {
            DbSet.Remove(entity);
            _context.SaveChanges();
        }
    }
}
