﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EfsolAspNetProject.Models;
using Microsoft.EntityFrameworkCore;

namespace EfsolAspNetProject
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<PointOfSale> PointsOfSale { get; set; }
        public DbSet<City> Cities { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
    }
}
